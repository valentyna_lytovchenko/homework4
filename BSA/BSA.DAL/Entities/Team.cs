﻿using System;

namespace BSA.DAL.Entities
{
    public class Team: BaseEntity
    {
        public string Name { get; set; }
    }
}
