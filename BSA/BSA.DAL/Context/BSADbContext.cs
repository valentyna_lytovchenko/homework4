﻿using BSA.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BSA.DAL.Context
{
    public class BSADbContext : DbContext 
    {
        public BSADbContext(DbContextOptions<BSADbContext> options) 
            : base(options) 
        { }

        public DbSet<User> Users { get; private set; }

        public DbSet<Team> Teams { get; private set; }

        public DbSet<Task> Tasks { get; private set; }

        public DbSet<Project> Projects { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            
            modelBuilder.Seed();
        }
    }
}
