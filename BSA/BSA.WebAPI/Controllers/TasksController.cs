﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] TaskDTO task)
        {
            _taskService.CreateTask(task);
            return Ok();
        }

        [HttpPut]
        public IActionResult Put([FromBody] TaskDTO task)
        {
            _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}
