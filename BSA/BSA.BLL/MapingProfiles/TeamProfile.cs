﻿using AutoMapper;
using BSA.Common.DTO;
using BSA.DAL.Entities;

namespace BSA.BLL.MapingProfiles
{
    public sealed class TeamProfile: Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>();
        }
    }
}
