﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<TeamDTO> GetTeams()
        {
            var teams = _context.Teams;
            return _mapper.Map<IList<TeamDTO>>(teams);
        }

        public TeamDTO GetTeamById(int id)
        {
            var team = _context.Teams.Where(t => t.Id == id);
            return _mapper.Map<TeamDTO>(team);
        }

        public void CreateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _context.Teams.Add(team);
            _context.SaveChanges();
        }

        public void DeleteTeam(int id)
        {
            var team = _context.Teams.Where(t => t.Id == id).FirstOrDefault();
            _context.Teams.Remove(team);
            _context.SaveChanges();
        }

        public void UpdateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _context.Teams.Update(team);
            _context.SaveChanges();
        }
    }
}
