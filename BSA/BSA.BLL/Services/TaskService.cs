﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<TaskDTO> GetTasks()
        {
            var tasks = _context.Tasks;
            return _mapper.Map<IList<TaskDTO>>(tasks);
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _context.Tasks.Where(t => t.Id == id);
            return _mapper.Map<TaskDTO>(task);
        }

        public void CreateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }

        public void DeleteTask(int id)
        {
            var task = _context.Tasks.Where(t => t.Id == id).FirstOrDefault();
            _context.Tasks.Remove(task);
            _context.SaveChanges();
        }

        public void UpdateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            _context.Tasks.Update(task);
            _context.SaveChanges();
        }
    }
}
