﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<ProjectDTO> GetProjects()
        {
            var projects = _context.Projects;
            return _mapper.Map<IList<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProjectById(int id)
        {
            var project = _context.Projects.Where(p => p.Id == id);
            return _mapper.Map<ProjectDTO>(project);
        }

        public void CreateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _context.Projects.Add(project);
            _context.SaveChanges();
        }

        public void DeleteProject(int id)
        {
            var project = _context.Projects.Where(p => p.Id == id).FirstOrDefault();
            _context.Projects.Remove(project);
            _context.SaveChanges();
        }

        public void UpdateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _context.Projects.Update(project);
            _context.SaveChanges();
        }
    }
}
