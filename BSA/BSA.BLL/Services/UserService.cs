﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using System.Collections.Generic;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using BSA.DAL.Context;
using System.Linq;

namespace BSA.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public IList<UserDTO> GetUsers()
        {
            var users =  _context.Users;
            return _mapper.Map<IList<UserDTO>>(users);
        }

        public UserDTO GetUserById(int id)
        {
            var user = _context.Users.Where(u => u.Id ==id);
            return _mapper.Map<UserDTO>(user);
        }

        public void CreateUser(UserDTO userDto)
        {
            var user = _mapper.Map<User>(userDto);
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            var user = _context.Users.Where(u => u.Id == id).FirstOrDefault();
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public void UpdateUser(UserDTO userDto)
        {
            var user = _mapper.Map<User>(userDto);
            _context.Users.Update(user);
            _context.SaveChanges();
        }
    }
}
